package com.example.demo.repositorio;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entidades.TblTarea;


@Repository
public interface TareaRepository extends CrudRepository<TblTarea, Integer> {

	
}
