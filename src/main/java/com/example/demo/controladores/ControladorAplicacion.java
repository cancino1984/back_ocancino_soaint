package com.example.demo.controladores;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entidades.TblTarea;
import com.example.demo.service.TareaServiceImpl;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
public class ControladorAplicacion {
 
@Autowired
private TareaServiceImpl tareaServiceImpl;

@GetMapping("/tareas")
@ApiOperation(value = "Listado de tareas", notes = "Obtiene el listado completo de las tareas existentes en el sistema.")
 public List<TblTarea> home() {
	List<TblTarea> list = tareaServiceImpl.getAll();
	return list;
 }

@PutMapping("/tarea")
@ApiOperation(value = "Inserta una nueva tarea", notes = "Inserta una nueva tarea al sistema.")
public TblTarea createTask(@Valid @RequestBody TblTarea tarea) {
    return tareaServiceImpl.addTask(tarea);
}

@PutMapping("/tarea/{id}")
@ApiOperation(value = "Edita una tarea existente", notes = "Edita una tarea ya existente en el sistema.")
public TblTarea editTask(@PathVariable int id, @Valid @RequestBody TblTarea tarea) throws Exception {
    return tareaServiceImpl.editTask(id, tarea);
}

@DeleteMapping("/tarea/{id}")
@ApiOperation(value = "Elimina una tarea existente", notes = "Elimina una tarea ya existente en el sistema.")
public ResponseEntity<?> deleteTask(@PathVariable int id) throws Exception {
    return tareaServiceImpl.deleteTask(id);
}

}