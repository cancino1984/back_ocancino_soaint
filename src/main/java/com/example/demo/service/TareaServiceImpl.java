package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.entidades.TblTarea;
import com.example.demo.repositorio.TareaRepository;
import javax.validation.Valid;

@Service
public class TareaServiceImpl implements TareaService {

	@Autowired
	private TareaRepository tareaRepository;

	@Override
	public List<TblTarea> getAll() {
		List<TblTarea> out = (List<TblTarea>) tareaRepository.findAll();
		out.stream().forEach(s -> {
			s.setId(s.getId());
			s.setDescripcion(s.getDescripcion());
			s.setFecha(s.getFecha());
			s.setVigente(s.getVigente());
		}); 
		return out;
	}
	
	@Override
	public TblTarea addTask(TblTarea tarea) {
		return tareaRepository.save(tarea);
	}
	
	@Override
	public TblTarea editTask(int id, TblTarea tarea) throws Exception {
		
		return tareaRepository.findById(id).map(s -> {
			s.setDescripcion(tarea.getDescripcion());
			s.setVigente(tarea.getVigente());
			return tareaRepository.save(s);
		}).orElseThrow(()-> new Exception("ERROR: Al editar el registro: " + id));
	}
	
	@Override
	public ResponseEntity<?> deleteTask(int id) throws Exception {
		
		return tareaRepository.findById(id).map(s -> {
				tareaRepository.delete(s);
				return ResponseEntity.ok().build();
		}).orElseThrow(()-> new Exception("ERROR: Al eliminar el registro: " + id));
	}	
	
}
