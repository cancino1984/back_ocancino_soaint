package com.example.demo.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.example.demo.entidades.TblTarea;

public interface TareaService {

	public List<TblTarea> getAll();

	public TblTarea addTask(TblTarea tarea);
	
	public TblTarea editTask(int id, TblTarea tarea) throws Exception;
	
	public ResponseEntity<?> deleteTask(int id) throws Exception; 
	
}
