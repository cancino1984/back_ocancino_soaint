package com.example.demo.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "TBL_TAREA")
@Entity
public class TblTarea extends auditoriaTbl {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = -2411095523942139531L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@NotNull(message = "Por favor especificar una descrpción de la tarea.")
	@Column(name = "DESCRIPCION", nullable = false)
	private String descripcion;
	
	@NotNull(message = "Por favor especificar si la tarea está vigente.")
	@Column(name = "VIGENCIA", nullable = false)
	private Boolean vigente;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getVigente() {
		return vigente;
	}

	public void setVigente(Boolean vigente) {
		this.vigente = vigente;
	}
}
