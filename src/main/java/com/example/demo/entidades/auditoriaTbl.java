package com.example.demo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"fecha"},
        allowGetters = true
)
public abstract class auditoriaTbl implements Serializable {
	
	/**
	 * Serial
	 */
	private static final long serialVersionUID = -9145829548584928601L;
	
	@Column(name = "FECHA_CREACION", updatable = false, nullable = false)
	@Temporal(TemporalType.DATE)
	@CreatedDate
	private Date fecha;

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
