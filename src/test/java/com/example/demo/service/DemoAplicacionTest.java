package com.example.demo.service;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.controladores.ControladorAplicacion;
import com.example.demo.repositorio.TareaRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoAplicacionTest {
    
	@Autowired
	private ControladorAplicacion  controladorAplicacion ;
	
	@Autowired
	private TareaRepository TareaRepository;
	
	@Autowired
	private TareaServiceImpl TareaServiceImpl;
	
	@Test
	public void contextLoads() {
		
		assertNotNull(controladorAplicacion.home());
		assertNotNull(TareaRepository.findAll());
		assertNotNull(TareaServiceImpl.getAll());
	}

}
